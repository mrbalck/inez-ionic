import { Injectable } from "@angular/core";
import { Storage } from '@ionic/storage';
import { Guid } from "guid-typescript";

@Injectable({
    providedIn: 'root'
})
export class UserManagement {

    constructor(private storage: Storage) { }

    public getUserID() {

        return new Promise(resolve => {
            
            this.storage.get("userId").then((userKey: String) => {

                if(userKey == null) {

                    userKey = this.generateUserID();
                    this.setUserID(userKey);
                }

                resolve(userKey);

            });
        });
    }

    private generateUserID(): String {

        return Guid.create().toString();

    }

    private setUserID(id: String): void {

        this.storage.set("userId", id);

    }
}
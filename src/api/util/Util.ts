export class Util {

    public static removeEmptyEntries(list: string[]) {

        for(let i = 0; i < list.length; i++) {
            let str = list[i];

            if(str == "") list.splice(i,1);

        }
    }

    public static getFormattedDate(date: Date): string {

        var day = date.getDate().toString().length == 1 ? "0" + date.getDate() : date.getDate();
        var month = date.getMonth().toString().length == 1 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
        var year = date.getFullYear();

        return day + "." + month + "." + year;

    }
}
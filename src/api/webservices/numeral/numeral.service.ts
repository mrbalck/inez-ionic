import { StandardRestService } from '../service';
import { Numeral } from 'src/api/models/Numeral';
import { RestConfiguration } from '../config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class NumeralService extends StandardRestService<Numeral> {

    constructor(config: RestConfiguration, http: HttpClient, toastController: ToastController) {
        super("/numerals", config, http, toastController);
    
    }
}
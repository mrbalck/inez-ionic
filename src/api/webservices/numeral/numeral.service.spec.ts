import { TestBed } from '@angular/core/testing';

import { NumeralService } from './numeral.service';

describe('NumeralService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NumeralService = TestBed.get(NumeralService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { RestConfiguration } from '../config';
import { Branch } from 'src/api/models/Branch';
import { HttpClient } from "@angular/common/http";
import { Location } from "src/api/models/Location";
import { RestService, StandardRestService } from '../service';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class BranchService extends StandardRestService<Branch> {

  constructor(config: RestConfiguration, http: HttpClient, toastController: ToastController) { 
    super("/branches", config, http, toastController);

  }

  public getClosestBranch(location: Location) {

    return new Promise(resolve => {
      this.http.get(this.getRepoUrl() + "/closest", { 
        params: {
          lat: String(location.lat),
          lng: String(location.lng)
      }}).subscribe(data => {

        resolve(data);

      }, error => {
        
        console.log(error);

      });
    });
  }
}

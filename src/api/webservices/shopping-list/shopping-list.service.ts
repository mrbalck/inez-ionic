import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestConfiguration } from '../config';
import { Branch } from 'src/api/models/Branch';
import { ShoppingList } from 'src/api/models/ShoppingsList';
import { StandardRestService } from '../service';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService extends StandardRestService<ShoppingList> {

  constructor(config: RestConfiguration, http: HttpClient, toastController: ToastController) { 
    super("/shoppinglists", config, http, toastController);

  }

  public getShoppingListsByUser(userId: string) {

    return new Promise(resolve => {

      this.http.get(this.getRepoUrl(), { 
        params: {
          userId: userId
      }}).subscribe((shoppingLists: ShoppingList[]) => {
        
        resolve(shoppingLists);

      },  error => {
            
        console.log(error);

      });
    });
  }

  public getShoppingListByUser(userId: string, listId: number) {

    return new Promise(resolve => {

      this.http.get(this.getRepoUrl() + "/" + listId, { 
        params: {
          userId: userId
      }}).subscribe((shoppingLists: ShoppingList[]) => {
        
        resolve(shoppingLists);

      },  error => {
            
        console.log(error);

      });
    });
  }

  public getComingShoppingListsByUser(userId: string) {

    return new Promise(resolve => {

      this.http.get(this.getRepoUrl() + "/coming", { 
        params: {
          userId: userId
      }}).subscribe((shoppingLists: ShoppingList[]) => {
        
        resolve(shoppingLists);

      },  error => {
            
        console.log(error);

      });
    });
  }

  public getUndatedShoppingListsByUser(userId: string) {

    return new Promise(resolve => {

      this.http.get(this.getRepoUrl() + "/undated", { 
        params: {
          userId: userId
      }}).subscribe((shoppingLists: ShoppingList[]) => resolve(shoppingLists),  error => console.log(error));
    });
  }

  public getExpiredShoppingListsByUser(userId: string) {

    return new Promise(resolve => {

      this.http.get(this.getRepoUrl() + "/expired", { 
        params: {
          userId: userId
      }}).subscribe((shoppingLists: ShoppingList[]) => resolve(shoppingLists),  error => console.log(error));
    });
  }

  public getPinnedShoppingList(userId: string) {

    return new Promise(resolve => {
      this.http.get(this.getRepoUrl() + "/pinned", { 
        params: {
          userId: userId
      }}).subscribe((shoppingList: ShoppingList) => resolve(shoppingList),  error => console.log(error));
    });
  }
}

import { Injectable } from '@angular/core';
import { RestConfiguration } from '../config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StandardRestService } from '../service';
import { Location } from "../../models/Location";
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LocationService extends StandardRestService<Location> {

  constructor(config: RestConfiguration, http: HttpClient, toastController: ToastController) {
    super("/locations", config, http, toastController);
    
  }
}

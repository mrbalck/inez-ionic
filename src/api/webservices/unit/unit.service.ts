import { Injectable } from '@angular/core';
import { StandardRestService } from '../service';
import { Unit } from 'src/api/models/Unit';
import { RestConfiguration } from '../config';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class UnitService extends StandardRestService<Unit> {

  constructor(protected config: RestConfiguration, protected http: HttpClient, toastController: ToastController) {
    super("/units", config, http, toastController);

  }

  public addUnitToUnitSystem(unit: Unit, unitSystemId: number) {

    return new Promise(resolve => {

      this.http.post(this.getRepoUrl() + "/add", unit, { 
        params: {
          unitSystem: String(unitSystemId)
      }}).subscribe(data => {

        resolve(data);

      }, error => {
        
        console.log(error);

      });
    });
  }

  public addSpecialUnitToUnitSystem(specialUnit, unitSystemId: number) {

    return new Promise(resolve => {

      this.http.post(this.getRepoUrl() + "/add", specialUnit, { 
        params: {
          unitSystem: String(unitSystemId)
      }}).subscribe(data => {

        resolve(data);

      }, error => {
        
        console.log(error);

      });
    });
  }
}

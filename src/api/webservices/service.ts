import { RestConfiguration } from './config';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';

export abstract class RestService {

    protected http: HttpClient;

    protected url: string;

    constructor(url: string, protected config: RestConfiguration, protected _http: HttpClient) {

        this.url = url;
        //Just for readablity
        this.http = _http;

    }

    public getRepoUrl(): string {

        return this.config.getApiPath() + this.url;

    }
}

export abstract class StandardRestService<T> extends RestService {

    protected toastController;

    constructor(url: string, protected config: RestConfiguration, protected _http: HttpClient, toastController: ToastController) {
        super(url, config, _http);

        this.toastController = toastController;

    }

    public getEntities() {

        return new Promise(resolve => {
            this.http.get(this.getRepoUrl()).subscribe(data => {
        
                resolve(data);
        
            }, error =>  {
        
                this.presentToast(error);
                console.log(error);
        
            });
        });
    }

    public getEntity(id: number) {

        return new Promise(resolve => {
            this.http.get(this.getRepoUrl() + "/" + id).subscribe(data => {
        
                resolve(data);
        
            }, error => {
                this.presentToast(error);
                console.log(error);
        
            });
        });
    }

    public removeEntity(id: number) {

        return new Promise(resolve => {
            this.http.delete(this.getRepoUrl() + "/" + id).subscribe(data => {

                resolve(data);
            
            }, error => {
                
                this.presentToast(error);
                console.log(error);
        
            });
        });
    }

    public addEntity(entity: T) {

        return new Promise(resolve => {
            this.http.post(this.getRepoUrl() + "/add", entity).subscribe(data => {
            
                resolve(data);
            
            }, error => {
                
                this.presentToast(error);
                console.log(error);
        
            });
        });
    }

    public updateEntity(entity: T) {

        return this.addEntity(entity);

    }

    async presentToast(msg: string) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 4000
        });
        toast.present();
    }
}
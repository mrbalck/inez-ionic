import { Injectable } from '@angular/core';
import { StandardRestService, RestService } from '../service';
import { RestConfiguration } from '../config';
import { HttpClient } from '@angular/common/http';
import { Product } from 'src/api/models/Product';
import { ListedProduct } from 'src/api/models/ListedProduct';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends RestService {

  constructor(config: RestConfiguration, http: HttpClient) {
    super("/products", config, http);

  }

  public getAllProducts() {

    return new Promise(resolve => {
      this.http.get(this.getRepoUrl()).subscribe(data => {
  
          resolve(data);
  
      }, error =>  {
  
          console.log(error);
  
      });
    });
  }

  public getListedProducts() {

    return new Promise(resolve => {
      this.http.get(this.getRepoUrl() + "/listed").subscribe(data => {
  
          resolve(data);
  
      }, error =>  {
  
          console.log(error);
  
      });
    });
  }

  public getUnlistedProducts() {

    return new Promise(resolve => {
      this.http.get(this.getRepoUrl() + "/unlisted").subscribe(data => {
  
          resolve(data);
  
      }, error =>  {
  
          console.log(error);
  
      });
    });
  }

  public addProduct(product: Product) {

    return new Promise(resolve => {
      this.http.post(this.getRepoUrl() + "/add", product).subscribe(data => {

        resolve(data);
    
        }, error => {
        
        console.log(error);

      });
    });
  }

  public addListedProduct(listedProduct: ListedProduct) {
    
    return new Promise(resolve => {
      this.http.post(this.getRepoUrl() + "/listed/add", listedProduct).subscribe(data => {

        resolve(data);
    
        }, error => {
        
        console.log(error);

      });
    });
  }

  public removeProduct(id: number) {

    return new Promise(resolve => {
      this.http.delete(this.getRepoUrl() + "/" + id).subscribe(data => {

        resolve(data);
    
        }, error => {
        
        console.log(error);

      });
    });
  }

  public getProduct(id: number) {

    return new Promise(resolve => {
      this.http.get(this.getRepoUrl() + "/" + id).subscribe(data => {
  
          resolve(data);
  
      }, error => {
          
          console.log(error);
  
      });
    });
  }

  public getListedProduct(id: number) {

    return new Promise(resolve => {
      this.http.get(this.getRepoUrl() + "/listed/" + id).subscribe(data => {
  
          resolve(data);
  
      }, error => {
          
          console.log(error);
  
      });
    });
  }
}

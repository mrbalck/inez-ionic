import { Injectable } from '@angular/core';
import { RestService } from '../service';
import { RestConfiguration } from '../config';
import { HttpClient } from '@angular/common/http';
import { ProductEntry } from 'src/api/models/ProductEntry';

@Injectable({
  providedIn: 'root'
})
export class SuggestionService extends RestService {

  constructor(config: RestConfiguration, http: HttpClient) { 
    super("/suggestions", config, http);

  }

  public getBestSuggestedProducts(input: string) {

    return new Promise(resolve => {
      this.http.get(this.getRepoUrl(), { 
        params: {
          input: input
      }}).subscribe((productEntries: ProductEntry[]) => {
  
          resolve(productEntries);
  
      }, error =>  {
  
          console.log(error);
  
      });
    });
  }

  public getSuggestedProducts(input: string) {

    return new Promise(resolve => {
      this.http.get(this.getRepoUrl() + "/all", { 
        params: {
          input: input
      }}).subscribe((productEntries: ProductEntry[]) => {
  
          resolve(productEntries);
  
      }, error =>  {
  
          console.log(error);
  
      });
    });
  }
}

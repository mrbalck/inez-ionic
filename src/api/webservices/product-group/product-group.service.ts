import { Injectable } from '@angular/core';
import { ProductGroup } from 'src/api/models/ProductGroup';
import { StandardRestService } from '../service';
import { HttpClient } from '@angular/common/http';
import { RestConfiguration } from '../config';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ProductGroupService extends StandardRestService<ProductGroup> {

  constructor(config: RestConfiguration, http: HttpClient, toastController: ToastController) { 
    super("/productgroups", config, http, toastController);

  }

  public getGroupsByIds(ids: number[]) {

    return new Promise(resolve => {
        this.http.post(this.getRepoUrl(), ids).subscribe(data => {

            resolve(data);
        
            }, error => {
            
            console.log(error);

        });
    });
  }
}

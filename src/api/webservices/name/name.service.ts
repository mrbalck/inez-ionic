import { Injectable } from '@angular/core';
import { StandardRestService, RestService } from '../service';
import { RestConfiguration } from '../config';
import { HttpClient } from '@angular/common/http';
import { Name } from 'src/api/models/Name';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NameService extends StandardRestService<Name> {

  constructor(config: RestConfiguration, http: HttpClient, toastController: ToastController) {
    super("/names", config, http, toastController);

  }
}

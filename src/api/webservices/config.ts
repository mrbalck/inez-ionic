import { Injectable } from "@angular/core";
import { Storage } from '@ionic/storage';

@Injectable({
    providedIn: "root"
})
export class RestConfiguration {

    server = "http://localhost:8080";

    apiRoot = "";

    constructor(private storage: Storage) {

        this.getServerIP().then((ip: string) => this.server = "http://" + ip);

    }

    public getApiPath(): string {

        return this.server + this.apiRoot;

    }

    public getServerIP() {

        return new Promise(resolve => {
            
            this.storage.get("serverIp").then((ip: String) => {

                if(ip == null) {

                    this.setServerIP("localhost:8080");

                    resolve("localhost:8080");

                } else {

                    resolve(ip);

                }
            });
        });
    }

    public setServerIP(ip: String): void {

        this.storage.set("serverIp", ip);

    }
}
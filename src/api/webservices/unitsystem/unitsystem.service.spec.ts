import { TestBed } from '@angular/core/testing';
import { UnitSystemService } from './unitsystem.service';


describe('UnitSystemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UnitSystemService = TestBed.get(UnitSystemService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { StandardRestService } from '../service';
import { Unit } from 'src/api/models/Unit';
import { RestConfiguration } from '../config';
import { HttpClient } from '@angular/common/http';
import { UnitSystem } from 'src/api/models/UnitSystem';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class UnitSystemService extends StandardRestService<UnitSystem> {

    constructor(protected config: RestConfiguration, protected http: HttpClient, toastController: ToastController) {
        super("/unitsystems", config, http, toastController);

    }
}

export class Name {

    id: number;

    singular: string = "";

    plural: string = "";

    constructor(id?: number) {

        if(id != undefined) this.id = id;

    }

    public toString(): string {

        return "ID: " + this.id + ", Singular: " + this.singular + ", Plural: " + this.plural;

    }


    public getName(amount: number) {

        if(amount <= 1) 
            return this.singular;
        else
            return this.plural;

    }

    public static getName(name: Name, amount: number) {

        if(amount <= 1) 
            return name.singular;
        else
            return name.plural;

    }
}
import { Entry } from "./Entry";
import { Branch } from './Branch';
import { ProductEntry } from './ProductEntry';
import { ListedProduct } from './ListedProduct';

export class ShoppingList {

    id: number;

    name: string;

    date: Date;

    branch: Branch;

    userId: string;

    entries: Entry[] = [];

    pinned: boolean = false;

    constructor(json?) {

        if(json != undefined) {

            this.id = json.id;
            this.name = json.name;
            if(json.date != null)
                this.date = new Date(json.date[0], json.date[1], json.date[2]);
            this.branch = json.branch;
            this.userId = json.userId;
            this.entries = json.entries;
            this.pinned = json.pinned;

        }

    }

    public getPrize(): number {

        let prize: number = 0;

        for(let entry of this.entries) {


            if((<ProductEntry> entry).product != undefined) {

                let pe = <ProductEntry> entry;

                if((<ListedProduct> pe.product).prize != undefined) {

                    let listedProduct: ListedProduct = <ListedProduct> pe.product;

                    prize += listedProduct.prize;
                }
            }

        }

        return prize;

    }
}
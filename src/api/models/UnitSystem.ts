import { Unit } from './Unit';
import { SpecialUnit } from './SpecialUnit';

export class UnitSystem {

    id: number;

    name: string;

    units: Unit[];
    
    specialUnits: SpecialUnit[];

    baseUnit: Unit;
}
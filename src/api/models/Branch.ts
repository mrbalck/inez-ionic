import { Location } from "./Location";
import { Structure } from './Structure';

export class Branch {

    id: number;

    location: Location;

    name: string;

    structure: Structure;

    constructor(name?: string, location?: Location, structure?: Structure) {

        this.name = name;
        this.location = location;
        this.structure = structure;

    }
}
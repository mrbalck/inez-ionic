import { Entry } from "./Entry";
import { Product } from './Product';
import { Unit } from './Unit';
import { ListedProduct } from './ListedProduct';
import { Name } from './Name';

export class ProductEntry extends Entry {

    product: Product;

    unit: Unit;

    amount: number;

    constructor() {
        super("");

    }

    public static getName(productEntry: ProductEntry): string {

        return productEntry.amount + " " + (productEntry.unit != null ? Name.getName(productEntry.unit.preferedName, productEntry.amount) : '') + " " + Name.getName(productEntry.product.preferedName, productEntry.amount);

    }

    public static getPrize(productEntry): number {

        if(productEntry.product.prize != undefined) {
            let listedProduct: ListedProduct = productEntry.product;

            if(productEntry.unit != null)
                return productEntry.amount * productEntry.unit.factorToBaseUnit * listedProduct.prize;
            else
                return productEntry.amount * listedProduct.prize;

        } else {

            return 0;

        }
    }
}
import { OrderedGroup } from './OrderedGroup';

export class OrderedGrouping {

    id: number;

    orderedGroups: OrderedGroup[];

    constructor(orderedGroups?: OrderedGroup[]) {

        this.orderedGroups = orderedGroups;

    }
}
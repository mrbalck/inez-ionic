import { Name } from './Name';

export class Unit {

    id: number;

    preferedName: Name = new Name();

    factorToBaseUnit: number;

    names: Name[] = [];

}
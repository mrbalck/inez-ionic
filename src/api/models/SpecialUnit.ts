import { Unit } from './Unit';

export class SpecialUnit {

    id: number;

    name: string;

    compareableUnit: Unit;

    factor: number;

}
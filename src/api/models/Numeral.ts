export class Numeral {

    id: number;

    numericValue: number;

    stringValues: string[] = [];

    //Standard
    numeralClassification: NumeralClassification = NumeralClassification.CONSTANTS;

}

export enum NumeralClassification {

    POWER_OF_TEN, BASE, CONSTANTS, TEN_SERIES

}
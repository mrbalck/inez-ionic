import { ProductGroup } from './ProductGroup';
import { Unit } from './Unit';
import { UnitSystem } from './UnitSystem';
import { Name } from './Name';

export class Product {

    id: number;

    preferedName: Name = new Name();

    synonyms: Name[] = [];

    productGroup: ProductGroup;

    unitSystem: UnitSystem = null;

    constructor() { }

}
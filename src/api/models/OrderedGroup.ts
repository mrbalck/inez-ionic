import { ProductGroup } from './ProductGroup';

export class OrderedGroup {

    id: number;

    orderNumber: number;

    productGroup: ProductGroup;

    constructor(orderNumber?: number, productGroup?: ProductGroup) {

        this.orderNumber = orderNumber;
        this.productGroup = productGroup;

    }
}
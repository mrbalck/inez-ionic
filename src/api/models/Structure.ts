import { ProductGroup } from './ProductGroup';
import { OrderedGrouping } from './OrderedGrouping';

export class Structure {

    id: number;

    orderedGrouping: OrderedGrouping;

    constructor() { }
}
import { Component, OnInit } from '@angular/core';
import { Numeral } from 'src/api/models/Numeral';
import { NumeralService } from 'src/api/webservices/numeral/numeral.service';

@Component({
  selector: 'app-numerals',
  templateUrl: './numerals.component.html',
  styleUrls: ['./numerals.component.scss', "../home/admin.page.scss"],
})
export class NumeralsComponent implements OnInit {

  numeral: Numeral = new Numeral();

  numerals: Numeral[] = [];

  stringValues: string;

  constructor(private numeralService: NumeralService) { }

  ngOnInit() {

    this.setNumerals();

  }

  private setNumerals() {

    this.numeralService.getEntities().then((numerals: Numeral[]) => {

      this.numerals = numerals;

    });
  }

  public addNumeral(): void {

    this.numeral.stringValues = this.getStringValues();

    this.numeralService.addEntity(this.numeral).then(() => this.setNumerals());

  }

  public removeNumeral(id: number): void {

    this.numeralService.removeEntity(id).then(() => this.setNumerals());

  }


  private getStringValues(): string[] {

    return this.stringValues.split(";");

  }
}

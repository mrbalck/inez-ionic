import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/api/webservices/product/product.service'
import { Product } from 'src/api/models/Product';
import { ProductGroupService } from 'src/api/webservices/product-group/product-group.service'
import { UnitService } from 'src/api/webservices/unit/unit.service'
import { Unit } from 'src/api/models/Unit';
import { ProductGroup } from 'src/api/models/ProductGroup';
import { UnitSystem } from 'src/api/models/UnitSystem';
import { ListedProduct } from 'src/api/models/ListedProduct';
import { Name } from 'src/api/models/Name';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss', '../home/admin.page.scss'],
})
export class ProductsComponent implements OnInit {

  product: Product = new Product();

  unitSystem: UnitSystem = new UnitSystem();

  productGroup: ProductGroup = new ProductGroup();

  products: Product[] = [];

  synonyms: string;

  constructor(private productService: ProductService, private unitService: UnitService, private productGroupService: ProductGroupService) { }

  ngOnInit() {

    this.setProducts();

  }

  private setProducts(): void {

    this.productService.getUnlistedProducts().then((products: Product[]) => {

      this.products = products;

    });
  }

  public addProduct() {

    this.product.productGroup = this.productGroup;

    if(this.unitSystem.id > 0)
      this.product.unitSystem = this.unitSystem;
    
    let synonyms = this.getSynonyms();
    synonyms.push(this.product.preferedName);

    this.product.synonyms = synonyms;

    this.productService.addProduct(this.product).then(() => this.setProducts());

  }

  public getSynonyms(): Name[] {

    let synonymsIds: number[] = this.parseSynonyms();

    let synonyms: Name[] = [];

    for(var i in synonymsIds) {

      synonyms.push(new Name(synonymsIds[i]));

    }

    return synonyms;
  }

  public parseSynonyms(): number[] {

    let synonymsIds: number[] = [];

    if(this.synonyms != undefined) synonymsIds = this.synonyms.split(";").map(Number)

    return synonymsIds;

  }

  public removeProduct(id: number): void {

    this.productService.removeProduct(id).then(() => this.setProducts());

  }
}

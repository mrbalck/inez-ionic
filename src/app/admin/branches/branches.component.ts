import { Component, OnInit } from '@angular/core';
import { Branch } from 'src/api/models/Branch';
import { BranchService } from 'src/api/webservices/branch/branch.service'
import { Location } from "src/api/models/Location";
import { ProductGroup } from 'src/api/models/ProductGroup';
import { ProductGroupService } from 'src/api/webservices/product-group/product-group.service'
import { Structure } from 'src/api/models/Structure';
import { OrderedGroup } from 'src/api/models/OrderedGroup';
import { OrderedGrouping } from 'src/api/models/OrderedGrouping';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.scss', '../home/admin.page.scss'],
})
export class BranchesComponent implements OnInit {

  branches: Branch[];

  groups: string;

  branchName: string;

  location = new Location();

  constructor(public branchService: BranchService, public productGroupService: ProductGroupService) { }


  ngOnInit() {

    this.setBranches();
  }

  private setBranches() {

    this.branchService.getEntities().then((branches: Branch[]) => {

      this.branches = branches;

    });
  }

  public removeBranch(id: number) {

    this.branchService.removeEntity(id).then(() => this.setBranches());

  }

  public async addBranch() {

    let structure = new Structure();

    let productGroupIds = this.parseGroupIds();

    let productGroups = <ProductGroup[]>await this.productGroupService.getGroupsByIds(productGroupIds);

    structure.orderedGrouping = this.getOrderedGrouping(productGroups);

    let branch = new Branch(this.branchName, this.location, structure);

    this.branchService.addEntity(branch).then(() => this.setBranches());

  }

  private parseGroupIds(): number[] {
    let ids = this.groups.split(";").map(Number);

    return ids;

  }

  private getOrderedGrouping(productGroups): OrderedGrouping {

    let orderedGroups = this.getOrderedGroups(productGroups);

    return new OrderedGrouping(orderedGroups);
    
  }

  private getOrderedGroups(productGroups: ProductGroup[]): OrderedGroup[] {

    let orderedGroups = [];

    for(let i in productGroups) {
      console.log("new");
      var productGroup = productGroups[i];

      orderedGroups.push(new OrderedGroup(parseInt(i), productGroup));

    }

    return orderedGroups;

  }
}

import { Component, OnInit } from '@angular/core';
import { ProductGroup } from 'src/api/models/ProductGroup';
import { ProductGroupService } from 'src/api/webservices/product-group/product-group.service'

@Component({
  selector: 'app-groups',
  templateUrl: './product-groups.component.html',
  styleUrls: ['./product-groups.component.scss', '../home/admin.page.scss'],
})
export class ProductGroupsComponent implements OnInit {

  productGroups: ProductGroup[];

  productGroup = new ProductGroup();

  constructor(private productGroupService: ProductGroupService) { }

  ngOnInit() {

    this.setProductGroups();

  }


  private setProductGroups() {

    this.productGroupService.getEntities().then((productGroups: ProductGroup[]) => {

      this.productGroups = productGroups;

    })
  }

  public addProductGroup() {

    this.productGroupService.addEntity(this.productGroup).then(() => this.setProductGroups());

  }

  public deleteProductGroup(id: number) {

    this.productGroupService.removeEntity(id).then(() => this.setProductGroups());

  }
}

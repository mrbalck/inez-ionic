import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/api/webservices/product/product.service'
import { ListedProduct } from 'src/api/models/ListedProduct';
import { Unit } from 'src/api/models/Unit';
import { ProductGroup } from 'src/api/models/ProductGroup';
import { UnitSystem } from 'src/api/models/UnitSystem';
import { Name } from 'src/api/models/Name';

@Component({
  selector: 'app-listed-products',
  templateUrl: './listed-products.component.html',
  styleUrls: ['./listed-products.component.scss', '../home/admin.page.scss'],
})
export class ListedProductsComponent implements OnInit {

  listedProduct: ListedProduct = new ListedProduct();

  unitSystem: UnitSystem = new UnitSystem();

  productGroup: ProductGroup = new ProductGroup();

  listedProducts: ListedProduct[] = [];

  synonyms: string;

  constructor(private productService: ProductService) { }

  ngOnInit() {

    this.setListedProducts();

  }

  private setListedProducts(): void {

    this.productService.getListedProducts().then((listedProducts: ListedProduct[]) => {

      this.listedProducts = listedProducts;

    });
  }

  public addListedProduct(): void {

    this.listedProduct.unitSystem = this.unitSystem;
    this.listedProduct.productGroup = this.productGroup;
    this.listedProduct.synonyms = this.getSynonyms();

    this.productService.addListedProduct(this.listedProduct).then(() => this.setListedProducts());

  }

  public getSynonyms(): Name[] {

    let synonymsIds: number[] = this.parseSynonyms();

    let synonyms: Name[] = [];

    for(let synonymId of synonymsIds) {

      synonyms.push(new Name(synonymId));

    }

    return synonyms;
  }

  public parseSynonyms(): number[] {

    let synonymsIds: number[] = [];
    
    if(this.synonyms != undefined) synonymsIds = this.synonyms.split(";").map(Number);

    return synonymsIds;

  }

  public removeListedProduct(id: number): void {

    this.productService.removeProduct(id).then(() => this.setListedProducts());

  }
}

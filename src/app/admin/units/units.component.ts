import { Component, OnInit } from '@angular/core';
import { UnitService } from 'src/api/webservices/unit/unit.service'
import { Unit } from 'src/api/models/Unit';
import { Util } from 'src/api/util/Util';
import { Name } from 'src/api/models/Name';

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.scss', '../home/admin.page.scss'],
})
export class UnitsComponent implements OnInit {

  unit: Unit = new Unit();

  nameIds: string;

  units: Unit[] = [];

  constructor(private unitService: UnitService) { }

  ngOnInit() {

    this.setUnits();

  }

  private setUnits(): void {

    this.unitService.getEntities().then((units: Unit[]) => {

      this.units = units;

    });
  }

  public addUnit(): void {

    let nameIds = [];

    if(this.nameIds != undefined)
      nameIds = this.nameIds.split(";");

    Util.removeEmptyEntries(nameIds);

    let names = this.getNames();
    names.push(this.unit.preferedName);
    console.log(names);
    this.unit.names = names;

    this.unitService.addEntity(this.unit).then(() => this.setUnits());

  }

  public removeUnit(id: number): void {

    this.unitService.removeEntity(id).then(() => this.setUnits());

  }

  private getNames(): Name[] {

    let nameIds: number[] = this.parseNames();

    let names = [];

    for(let nameId of nameIds) {

      names.push(new Name(nameId));

    }

    return names;

  }

  private parseNames(): number[] {

    let nameIds: number[] = [];

    if(this.nameIds != undefined)
      this.nameIds.split(";").map(Number);

    return nameIds;

  }

}

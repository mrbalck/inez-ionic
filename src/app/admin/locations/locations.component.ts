import { Component, OnInit } from '@angular/core';
import { LocationService } from 'src/api/webservices/location/location.service'
import { Location } from "src/api/models/Location";

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss', '../home/admin.page.scss'],
})
export class LocationsComponent implements OnInit {

  locations: Location[];

  location = new Location();

  constructor(public locationService: LocationService) { }

  ngOnInit() {

    this.setLocations();
  }

  public addLocation() {

    this.locationService.addEntity(this.location).then(() => this.setLocations());

  }

  public removeLocation(id: number) {

    this.locationService.removeEntity(id).then(() => this.setLocations());

  }

  private setLocations() {

    this.locationService.getEntities().then((locations: Location[]) => {

      this.locations = locations;

    });
  }
}

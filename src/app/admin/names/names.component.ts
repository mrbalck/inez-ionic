import { Component, OnInit } from '@angular/core';
import { Name } from 'src/api/models/Name';
import { NameService } from 'src/api/webservices/name/name.service';

@Component({
  selector: 'app-names',
  templateUrl: './names.component.html',
  styleUrls: ['./names.component.scss', '../home/admin.page.scss'],
})
export class NamesComponent implements OnInit {

  name: Name = new Name();

  names: Name[] = [];

  constructor(private nameService: NameService) { }

  ngOnInit() {

    this.setNames();

  }

  private setNames(): void {

    this.nameService.getEntities().then((productNames: Name[]) => {

      this.names = productNames;

    });
  }

  public addName() {

    if(this.name.plural == undefined || this.name.plural == "") this.name.plural = this.name.singular;

    this.nameService.addEntity(this.name).then(() => this.setNames());

  }

  public removeName(id: number): void {

    this.nameService.removeEntity(id).then(() => this.setNames());

  }
}

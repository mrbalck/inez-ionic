import { Component, OnInit } from '@angular/core';
import { UserManagement } from 'src/api/util/UserManagement';
import { RestConfiguration } from 'src/api/webservices/config';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  userId: string;

  serverIp: string;

  constructor(private userManagement: UserManagement, private restConfig: RestConfiguration) { }

  ngOnInit() {

    this.userManagement.getUserID().then((id: string) => this.userId = id);

    this.restConfig.getServerIP().then((ip: string) => this.serverIp = ip);

  }

  public updateServerIP(): void {

    this.restConfig.setServerIP(this.serverIp);

  }
}

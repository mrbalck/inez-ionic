import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminPage } from './admin.page';
import { BranchesComponent } from '../branches/branches.component';
import { ProductGroupsComponent } from '../product-groups/product-groups.component';
import { ListedProductsComponent } from '../listed-products/listed-products.component';
import { LocationsComponent } from '../locations/locations.component';
import { ProductsComponent } from '../products/products.component';
import { UnitsComponent } from '../units/units.component';
import { UnitSystem } from 'src/api/models/UnitSystem';
import { UnitSystemsComponent } from '../unit-systems/unit-systems.component';
import { NamesComponent } from '../names/names.component';
import { NumeralsComponent } from '../numerals/numerals.component';

const routes: Routes = [
  { path: '', component: AdminPage },
  { path: 'branches', component: BranchesComponent },
  { path: 'groups', component: ProductGroupsComponent },
  { path: 'listed-products', component: ListedProductsComponent },
  { path: 'locations', component: LocationsComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'units', component: UnitsComponent },
  { path: 'unitSystems', component: UnitSystemsComponent },
  { path: 'names', component: NamesComponent },
  { path: 'numerals', component: NumeralsComponent }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPageRoutingModule { }
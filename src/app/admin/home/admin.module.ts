import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AdminPage } from './admin.page';
import { AdminPageRoutingModule } from './admin-routing.module';
import { BranchesComponent } from '../branches/branches.component';
import { ProductGroupsComponent } from '../product-groups/product-groups.component';
import { ListedProductsComponent } from '../listed-products/listed-products.component';
import { LocationsComponent } from '../locations/locations.component';
import { ProductsComponent } from '../products/products.component';
import { UnitsComponent } from '../units/units.component';
import { UnitSystemsComponent } from '../unit-systems/unit-systems.component';
import { NamesComponent } from '../names/names.component';
import { NumeralsComponent } from '../numerals/numerals.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminPageRoutingModule
  ],
  declarations: [
    AdminPage, 
    BranchesComponent, 
    ProductGroupsComponent, 
    ListedProductsComponent, 
    LocationsComponent,
    ProductsComponent,
    UnitsComponent,
    UnitSystemsComponent,
    NamesComponent,
    NumeralsComponent
  ],
  bootstrap: [AdminPage],
})
export class AdminPageModule {}

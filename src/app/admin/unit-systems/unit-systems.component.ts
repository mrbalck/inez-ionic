import { Component, OnInit } from '@angular/core';
import { UnitSystem } from 'src/api/models/UnitSystem';
import { Unit } from 'src/api/models/Unit';
import { UnitSystemService } from 'src/api/webservices/unitsystem/unitsystem.service';
import { SpecialUnit } from 'src/api/models/SpecialUnit';
import { UnitService } from 'src/api/webservices/unit/unit.service';

@Component({
  selector: 'app-unitsystems',
  templateUrl: './unit-systems.component.html',
  styleUrls: ['./unit-systems.component.scss', '../home/admin.page.scss'],
})
export class UnitSystemsComponent implements OnInit {

  unitSystems: UnitSystem[] = [];

  unitSystem: UnitSystem = new UnitSystem();

  baseUnit: Unit = new Unit();

  unit: Unit = new Unit();

  specialUnit: SpecialUnit = new SpecialUnit();

  unitSystemId: number;

  constructor(private unitService: UnitService, private unitSystemService: UnitSystemService) { }

  ngOnInit() {

    this.setUnitSystems();

  }

  private setUnitSystems(): void {

    this.unitSystemService.getEntities().then((unitSystems: UnitSystem[]) => this.unitSystems = unitSystems);
  }

  public addUnitSystem(): void {

    this.unitSystem.baseUnit = this.baseUnit;

    this.unitSystemService.addEntity(this.unitSystem).then(() => this.setUnitSystems());

  }

  public removeUnitSystem(id: number): void {

    this.unitSystemService.removeEntity(id).then(() => this.setUnitSystems());

  }

  public addUnitToUnitSystem(): void {

    this.unitService.addUnitToUnitSystem(this.unit, this.unitSystemId).then(() => this.setUnitSystems());

  }

  public addSpecialUnitToUnitSystem(): void {

    this.unitService.addSpecialUnitToUnitSystem(this.specialUnit, this.unitSystemId).then(() => this.setUnitSystems());

  }
}

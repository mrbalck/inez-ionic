import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./user/tabs/tabs.module').then(m => m.TabsPageModule),
  },
  { path: 'admin', loadChildren: './admin/home/admin.module#AdminPageModule' },
  { path: 'shopping-list', loadChildren: './user/shopping-list/shopping-list.module#ShoppingListPageModule' },
  { path: 'shopping-list/:id', loadChildren: './user/shopping-list/shopping-list.module#ShoppingListPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

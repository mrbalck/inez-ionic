import { Component, OnInit } from '@angular/core';
import { ShoppingList } from 'src/api/models/ShoppingsList';
import { ShoppingListService } from 'src/api/webservices/shopping-list/shopping-list.service';
import { UserManagement } from 'src/api/util/UserManagement';
import { Util } from 'src/api/util/Util';
import { Router, NavigationExtras } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-shopping-lists',
  templateUrl: './shopping-lists.page.html',
  styleUrls: ['./shopping-lists.page.scss'],
})
export class ShoppingListsPage implements OnInit {

  comingShoppingLists: ShoppingList[] = [];

  undatedShoppingLists: ShoppingList[] = [];

  expiredShoppingLists: ShoppingList[] = [];

  constructor(private shoppingListService: ShoppingListService, 
              private userManagement: UserManagement, 
              private router: Router,
              public toastController: ToastController) { }

  ngOnInit() {

    this.setLists();

  }

  private setLists() {

    this.setComingShoppingLists();
    this.setUndatedShoppingLists();
    this.setExpriedShoppingLists();

  }

  private setComingShoppingLists() {

    this.userManagement.getUserID().then((userId: string) => {

      this.shoppingListService.getComingShoppingListsByUser(userId).then((comingShoppingLists: ShoppingList[]) => this.comingShoppingLists = comingShoppingLists);

    });
  }

  private setUndatedShoppingLists() {

    this.userManagement.getUserID().then((userId: string) => {

      this.shoppingListService.getUndatedShoppingListsByUser(userId).then((undatedShoppingLists: ShoppingList[]) => this.undatedShoppingLists = undatedShoppingLists);
    
    });
  }

  private setExpriedShoppingLists() {

    this.userManagement.getUserID().then((userId: string) => {

      this.shoppingListService.getExpiredShoppingListsByUser(userId).then((expiredShoppingLists: ShoppingList[]) => this.expiredShoppingLists = expiredShoppingLists);
    
    });
  }

  public getRemainingDays(shoppingList: ShoppingList) {

    let d = new Date(shoppingList.date);
    let now = new Date(Date.now());
    const diffTime = Math.abs(d.getTime() - now.getTime());
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 

    return diffDays;

  }

  public getFormattedDate(date: Date): string {

    return Util.getFormattedDate(new Date(date));

  }

  public toDetailedShoppingList(shoppingList: ShoppingList) {

    this.router.navigate(['shopping-list/' + shoppingList.id]);

  }

  public removeShoppingList(shoppingList: ShoppingList) {

    this.shoppingListService.removeEntity(shoppingList.id).then(() => {
      this.presentToast("Einkaufsliste glöscht.");
      this.setLists();
    });

  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}

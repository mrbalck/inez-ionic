import { ShoppingListsPage } from './shopping-lists.page';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from '../components/components.module';
import { HomePage } from '../home/home.page';

const routes: Routes = [
    {
      path: '',
      component: ShoppingListsPage
    }
  ];
  
  @NgModule({
    imports: [
      CommonModule,
      FormsModule,
      IonicModule,
      RouterModule.forChild(routes),
      ComponentsModule
    ],
    declarations: [ShoppingListsPage]
  })
  export class ShoppingListsPageModule {}
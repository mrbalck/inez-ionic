import { Component, OnInit } from '@angular/core';
import { Branch } from 'src/api/models/Branch';
import { BranchService } from 'src/api/webservices/branch/branch.service';
import { Storage } from '@ionic/storage';
import { ShoppingList } from 'src/api/models/ShoppingsList';
import { ShoppingListService } from 'src/api/webservices/shopping-list/shopping-list.service';
import { UserManagement } from 'src/api/util/UserManagement';
import { Util } from 'src/api/util/Util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  selectedBranch: Branch;

  comingShoppingLists: ShoppingList[] = [];

  constructor(private branchService: BranchService, private storage: Storage, private shoppingListService: ShoppingListService, private userManagement: UserManagement, private router: Router) { }

  ngOnInit() {

    this.getSelectedBranch();
    this.setComingShoppingLists();

  }

  private getSelectedBranch(): void {

    this.storage.get("selectedBranchId").then((id: number) => {

      if(id != null) {

        this.branchService.getEntity(id).then((branch: Branch) => {

          this.selectedBranch = branch;

        });
      }
    });
  }

  private setComingShoppingLists() {

    this.userManagement.getUserID().then((userId: string) => {

      this.shoppingListService.getComingShoppingListsByUser(userId).then((comingShoppingLists: ShoppingList[]) => this.comingShoppingLists = comingShoppingLists);

    });
  }

  public getFormattedDate(shoppingList: ShoppingList): string {

    return Util.getFormattedDate(new Date(shoppingList.date));

  }

  public toDetailedShoppingList(shoppingList: ShoppingList) {

    this.router.navigate(['shopping-list/' + shoppingList.id]);

  }
}

import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';

import { ComingShoppingComponent } from './coming-shopping/coming-shopping.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule
    ],
    declarations: [
        ComingShoppingComponent
    ],
    exports: [
        ComingShoppingComponent
    ]
  })
  export class ComponentsModule {}
import { Component, OnInit } from '@angular/core';
import { UserManagement } from 'src/api/util/UserManagement';
import { ShoppingListService } from 'src/api/webservices/shopping-list/shopping-list.service';
import { ShoppingList } from 'src/api/models/ShoppingsList';
import { Util } from 'src/api/util/Util';
import { Router } from '@angular/router';

@Component({
  selector: 'coming-shopping',
  templateUrl: './coming-shopping.component.html',
  styleUrls: ['./coming-shopping.component.scss'],
})
export class ComingShoppingComponent implements OnInit {

  pinnedShoppingList: ShoppingList;

  constructor(private userManagment: UserManagement, private shoppingListService: ShoppingListService, private router: Router) { }

  ngOnInit() {

    this.setPinnedShoppingList();

  }

  private setPinnedShoppingList() {

    this.userManagment.getUserID().then((userId: string) => {

      this.shoppingListService.getPinnedShoppingList(userId).then((shoppingList: ShoppingList) => {

        if(shoppingList != null && shoppingList != undefined)
          this.pinnedShoppingList = new ShoppingList(shoppingList);
  
      });
    });
  }

  public getRemainingDays(shoppingList: ShoppingList) {

    let d = new Date(shoppingList.date);
    let now = new Date(Date.now());
    const diffTime = Math.abs(d.getTime() - now.getTime());
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 

    return diffDays;

  }

  public getFormattedDate(shoppingList: ShoppingList): string {

    return Util.getFormattedDate(new Date(shoppingList.date));

  }

  public toDetailedShoppingList() {

    this.router.navigate(['shopping-list/' + this.pinnedShoppingList.id]);

  }
}

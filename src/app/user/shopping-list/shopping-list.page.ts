import { Component, OnInit } from '@angular/core';
import { ProductEntry } from 'src/api/models/ProductEntry';
import { ShoppingList } from 'src/api/models/ShoppingsList';
import { SuggestionService } from 'src/api/webservices/suggestion/suggestion.service';
import { ShoppingListService } from 'src/api/webservices/shopping-list/shopping-list.service';
import { UserManagement } from 'src/api/util/UserManagement';
import { Entry } from 'src/api/models/Entry';
import { Name } from 'src/api/models/Name';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.page.html',
  styleUrls: ['./shopping-list.page.scss'],
})
export class ShoppingListPage implements OnInit {

  shoppingList: ShoppingList = new ShoppingList();

  showDetails: boolean = false;

  suggestions: ProductEntry[] = [];

  entryInput: string;

  prize = 0.0;

  constructor(private suggestionService: SuggestionService, 
              private shoppingListService: ShoppingListService, 
              private userManagment: UserManagement,
              private route: ActivatedRoute,
              public toastController: ToastController) {

    this.route.params.subscribe( params => {

      if(params.id != undefined) {
        
        userManagment.getUserID().then((userId: string) => {

          shoppingListService.getShoppingListByUser(userId, params.id).then((shoppingList: ShoppingList) => {

            this.shoppingList = new ShoppingList(shoppingList);

          });
        });
      }
    });
  }  

  ngOnInit() {
  }

  toggleDetails() {

    this.showDetails = !this.showDetails;

  }

  updatePrize() {

    this.prize = this.shoppingList.getPrize();
  }

  getProductCount() {

    return this.shoppingList.entries.length;

  }

  addProduct(productEntry: ProductEntry) {

    productEntry.name = ProductEntry.getName(productEntry);
    this.shoppingList.entries.push(productEntry);

    this.entryInput = "";

    this.updatePrize();

  }

  addEntry() {

    let entry = new Entry(this.entryInput);

    this.shoppingList.entries.push(entry);

    this.entryInput = "";

  }

  entryInputChanged() {

    this.setSuggestions(this.entryInput);

  }

  private setSuggestions(input: string) {

    this.suggestionService.getBestSuggestedProducts(input).then((productEntries: ProductEntry[]) => {
      
      this.suggestions = productEntries;

    });
  }

  getProductName(productEntry: ProductEntry): string {

    return Name.getName(productEntry.product.preferedName, productEntry.amount);

  }

  getUnitName(productEntry: ProductEntry): string {

    if(productEntry.unit != null)
      return Name.getName(productEntry.unit.preferedName, productEntry.amount);
    else
      return "";

  }

  getProductEntryPrize(productEntry: ProductEntry): number {

    return ProductEntry.getPrize(productEntry);

  }

  getEntryName(entry) {

    if(entry.product != undefined) {
      console.log("test");
      return ProductEntry.getName(entry);

    } else {
      console.log("test2");
      return entry.name;

    }
  }

  getEntryPrize(entry) {

    if(entry.product != undefined)
      return ProductEntry.getPrize(entry);

    return 0;
  }

  removeEntry(entry) {

    this.shoppingList.entries.splice(this.shoppingList.entries.indexOf(entry), 1);

  }

  public saveShoppingList(): void {

    this.userManagment.getUserID().then((userId: string) => {
     
      this.shoppingList.userId = userId;

      this.shoppingListService.addEntity(this.shoppingList).then(() => {
        
        this.presentToast("Einkaufsliste gespeichert.");

      });
    });
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
